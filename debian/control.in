Source: metaio
Section: lscsoft
Priority: optional
Maintainer: Kipp Cannon <kipp.cannon@ligo.org>
Build-Depends: debhelper (>= 7.0.50), zlib1g-dev
Standards-Version: 3.8.6

Package: libmetaio@SONAME@
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: LIGO Light-Weight XML I/O library
 This code implements a simple recursive-descent parsing scheme for LIGO_LW
 files, based on the example in Chapter 2 of "Compilers: Principles,
 Techniques and Tools" by Aho, Sethi and Ullman.  Written by Philip
 Charlton and Peter Shawhan.
 .
 This package contains the shared-object library needed to run libmetaio
 applications.

Package: libmetaio-dev
Architecture: any
Depends: libmetaio@SONAME@ (= ${binary:Version})
Breaks: libmetaio@SONAME@ (<< ${binary:Version})
Replaces: libmetaio@SONAME@ (<< ${binary:Version})
Description: LIGO Light-Weight XML I/O library
 This code implements a simple recursive-descent parsing scheme for LIGO_LW
 files, based on the example in Chapter 2 of "Compilers: Principles,
 Techniques and Tools" by Aho, Sethi and Ullman.  Written by Philip
 Charlton and Peter Shawhan.
 .
 This package contains the files needed for building libmetaio programs.

Package: libmetaio@SONAME@-dbg
Architecture: any
Section: debug
Priority: extra
Depends: libmetaio@SONAME@ (= ${binary:Version}), ${misc:Depends}
Description: debugging symbols for libmetaio
 This code implements a simple recursive-descent parsing scheme for LIGO_LW
 files, based on the example in Chapter 2 of "Compilers: Principles,
 Techniques and Tools" by Aho, Sethi and Ullman.  Written by Philip
 Charlton and Peter Shawhan.
 .
 This package contains the debugging symbols for libmetaio.

Package: libmetaio-matlab
Architecture: any
Depends: libmetaio@SONAME@ (= ${binary:Version})
Description: LIGO Light-Weight XML I/O library
 This package provides the MatLab readMeta module from libmetaio.

Package: libmetaio-utils
Architecture: any
Depends: libmetaio@SONAME@ (= ${binary:Version})
Description: LIGO Light-Weight XML I/O library
 This package provides the utilities, such as lwtprint, which accompany the
 libmetaio source code.
